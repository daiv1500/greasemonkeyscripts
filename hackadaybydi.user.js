// ==UserScript==
// @name        Hackaday layout
// @namespace   http://localhost
// @description Customises Hackaday
// @include     http*://hackaday.com/*
// @version     1
// @grant       GM_getValue
// @grant       GM_setValue
// @grant       GM_addStyle
// ==/UserScript==

document.body.style.background = "#fff";
document.body.style.color = "#000";

GM_addStyle('.widget-area { display: none !important;}');
GM_addStyle('.content-area { width: 100% !important; padding-top: 5em;} ');
GM_addStyle('.main-navigation a {	color: #000; }');
GM_addStyle('a { color: #4C9ED9 !important; }');
GM_addStyle('a:hover, a:focus, a:active { color: #4C9ED9 !important; }');

GM_addStyle('h1, h1 > a, h2, h2 > a {	font-weight: 800;	text-transform: uppercase;	color: #000 !important;	text-decoration: none; }');
GM_addStyle('.site-branding .site-title a {	color: #000; }');
GM_addStyle('.blog, .entry-header, .entry-content, .entry-footer, .entry-meta, .entry-date {	margin: 0; background-color: #fff; }');

GM_addStyle('.site-footer {	background-color: #fff;}');
GM_addStyle('#leaderboard { display: none; }');
GM_addStyle('.site-title { display: none; }');
GM_addStyle('#content { width: 90%; padding-top: 5em;}');
GM_addStyle('#masthead, #footer { width: 100%; padding-left: 10px; padding-right: 10px; background-color: #fff; }');
GM_addStyle('#masthead { position: fixed; z-index: 10000; top: 0px; }');
GM_addStyle('.store-item { background: #fff; }');
GM_addStyle('.trending-project { background: #fff; }');
GM_addStyle('pre, code {background: #eee;}');
GM_addStyle('.report-abuse,.site-branding { display: none !important; }');